# Nordsquare #

This is a sample app demonstrating how to build a REST client for Foursquare using Retrofit
and Mosby MVP.

### Notes ###

When building the app, replace the client ID and client secret in `Auth.java` with the credentials
you got from Foursquare.

Ensure your device has the latest Google Play Services installed.

### Details ###

Nordsquare is dependent of the following third-party libraries:

- Retrofit (for API access)

- Mosby MVP (for MVP)

- Smartlocation (for easy location fetching)

- Mockito (for test mock-ups)

The Mosby MVP forms the architectural core of Nordsquare. For simplicity's sake, the app consists
only of a single view, single presenter and a simple model structure.

The control flow of the app is rather simple: The `MainActivity` implements the `VenueView`
and is responsible for handling the user interaction. When new data is required, the activity
invokes the `VenuesPresenter.loadVenues()` with a search query. Once done, the presenter
informs the view of the new data, or about an error if something went wrong.

### Testing ###

The project contains two instrumentation tests which demonstrate how to set up such tests
using Mockito.