package com.manabreak.nordsquare;

import android.support.v4.widget.SwipeRefreshLayout;
import android.test.ActivityInstrumentationTestCase2;
import android.view.View;
import android.widget.TextView;

import junit.framework.TestCase;

/**
 * Created by Harri on 10.11.2015.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity>
{
    private MainActivity mActivity;

    public MainActivityTest()
    {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        mActivity = getActivity();
    }

    public void testCreatePresenter() throws Exception
    {
        assertNotNull(mActivity.createPresenter());
    }

    public void testShowContent() throws Exception
    {
        mActivity.showContent();

        TextView tvSearching = (TextView)mActivity.findViewById(R.id.loadingView);
        assertEquals(View.GONE, tvSearching.getVisibility());

        SwipeRefreshLayout srl = (SwipeRefreshLayout)mActivity.findViewById(R.id.contentView);
        assertFalse(srl.isRefreshing());
    }
}