package com.manabreak.nordsquare.presenters;

import android.content.Context;
import android.test.AndroidTestCase;
import android.test.InstrumentationTestCase;

import com.manabreak.nordsquare.models.Venue;
import com.manabreak.nordsquare.views.VenueView;

import junit.framework.TestCase;

import org.mockito.Matchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.after;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * Unit tests for the venue presenter
 *
 * Created by Harri Pellikka on 10.11.2015.
 */
public class VenuesPresenterTest extends InstrumentationTestCase
{
    public static class VenueViewImpl implements VenueView
    {
        @Override public void onNoLocationProvider() {}
        @Override public void onLocationServicesDisabled() {}
        @Override public void showLoading(boolean pullToRefresh) {}
        @Override public void showContent() {}
        @Override public void showError(Throwable e, boolean pullToRefresh) {}
        @Override public void setData(List<Venue> data) {}
        @Override public void loadData(boolean pullToRefresh) {}
    }

    private Context mContext;
    private VenueViewImpl mView;
    private VenuesPresenter mVP;

    public void setUp() throws Exception
    {
        // Workaround for Mockito cache
        System.setProperty("dexmaker.dexcache", getInstrumentation().getTargetContext().getCacheDir().getPath());

        mContext = getInstrumentation().getTargetContext();
        mView = mock(VenueViewImpl.class);

        mVP = new VenuesPresenter(mContext);
        mVP.attachView(mView);
    }

    public void tearDown() throws Exception
    {

    }

    public void testLoadVenuesNoSwipe() throws Exception
    {
        mVP.loadVenues("coffee", false);
        verify(mView, times(1)).showLoading(false);
        verify(mView, timeout(5000)).showContent();
    }

    public void testLoadVenuesSwipe() throws Exception
    {
        mVP.loadVenues("sushi", true);
        verify(mView, times(1)).showLoading(true);
        verify(mView, timeout(5000)).showContent();
    }

    public void testQueryCaching() throws Exception
    {
        mVP.loadVenues("gas", false);
        mVP.loadVenues("gas", false);
        verify(mView, times(1)).showLoading(false);
    }
}