package com.manabreak.nordsquare.models;

import java.util.List;

/**
 * Base model for responses from Foursquare API.
 *
 * Created by Harri Pellikka on 10.11.2015.
 */
public class SearchResponse
{
    /**
     * The response object inside the base response
     */
    public static class Response
    {
        private List<Venue> venues;
    }

    private Response response;

    /**
     * Retrieves the list of venues
     * @return List of venues
     */
    public List<Venue> getVenues()
    {
        return response.venues;
    }
}
