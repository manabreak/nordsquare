package com.manabreak.nordsquare.models;

/**
 * A model representing a single venue.
 * For the purposes of this code sample, this model
 * contains just the name and the location of the venue.
 *
 * Created by Harri Pellikka on 9.11.2015.
 */
public class Venue
{
    private String name;
    private VenueLocation location;

    /**
     * Retrieves the name of the venue
     * @return Name of the venue
     */
    public String getName()
    {
        return name;
    }

    /**
     * Retrieves the location of the venue
     * @return Location of the venue
     */
    public VenueLocation getLocation()
    {
        return location;
    }
}
