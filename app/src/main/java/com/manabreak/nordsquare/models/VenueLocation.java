package com.manabreak.nordsquare.models;

/**
 * Model representing the venue location.
 * While the actual data may contain much more info,
 * for the purposes of this app the location comprises only
 * of the address (if available), the lat-lon pair and the
 * distance (in meters).
 *
 * Created by Harri Pellikka on 10.11.2015.
 */
public class VenueLocation
{
    private String address;
    private double lat;
    private double lng;
    private double distance;

    /**
     * Retrieves the street address of the venue
     * @return Address
     */
    public String getAddress()
    {
        return address;
    }

    /**
     * Retrieves the latitude of this location
     * @return Latitude
     */
    public double getLatitude()
    {
        return lat;
    }

    /**
     * Retrieves the longitude of this location
     * @return Longitude
     */
    public double getLongitude()
    {
        return lng;
    }

    /**
     * Retrieves the distance to this location, in meters.
     * @return Distance (in meters) to this location
     */
    public double getDistance()
    {
        return distance;
    }

    /**
     * Returns a formatted presentation of the distance.
     * If the distance is less than one kilometer, the distance
     * will be shown in meters. If the distance is over one kilometer
     * but less than 100 km, the distance is shown in kilometers
     * with a single-decimal precision. If the distance is greater
     * than 100 km, no decimals are shown.
     *
     * @return Formatted representation of the distance
     */
    public String getFormattedDistance()
    {
        if(distance < 1000.0)
        {
            return "" + (int)distance + " m";
        }

        double d = distance / 1000.0;
        return String.format(d < 100.0 ? "%.1f km" : "%.0f km", d);
    }
}
