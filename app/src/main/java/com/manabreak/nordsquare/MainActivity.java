package com.manabreak.nordsquare;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.hannesdorfmann.mosby.mvp.lce.MvpLceActivity;
import com.manabreak.nordsquare.models.Venue;
import com.manabreak.nordsquare.presenters.VenuesPresenter;
import com.manabreak.nordsquare.views.VenueView;

import java.util.List;

/**
 * The main activity. Extends the MVP Loading-Content-Error activity and
 * implements the view for venues.
 */
public class MainActivity extends MvpLceActivity<SwipeRefreshLayout, List<Venue>, VenueView, VenuesPresenter>
    implements VenueView, SwipeRefreshLayout.OnRefreshListener
{
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private VenuesAdapter mAdapter;
    private EditText mSearchBox;
    private String mSearchQuery = "";
    private int PLAY_SERVICES_RESOLUTION_REQUEST = 1001;

    private void checkPlayServices()
    {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if(resultCode != ConnectionResult.SUCCESS)
        {
            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode))
            {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
            else
            {
                Toast.makeText(this, R.string.error_play_services, Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkPlayServices();

        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.contentView);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rvVenues);

        mAdapter = new VenuesAdapter();
        recyclerView.setAdapter(mAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        mSearchBox = (EditText)findViewById(R.id.etSearch);
        mSearchBox.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                // Check for illegal characters
                String i = s.toString().trim();
                if(i.matches("[a-zA-Z 0-9]+") || i.isEmpty())
                {
                    mSearchBox.setBackgroundResource(R.color.valid_input);
                    mSearchQuery = i;
                    loadData(false);
                }
                else
                {
                    mSearchBox.setBackgroundResource(R.color.error_input);
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        loadData(false);
    }

    @NonNull
    @Override
    public VenuesPresenter createPresenter()
    {
        return new VenuesPresenter(this);
    }

    @Override
    public void showContent()
    {
        super.showContent();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    protected String getErrorMessage(Throwable throwable, boolean b)
    {
        return getString(R.string.error_network);
    }

    @Override
    public void setData(List<Venue> data)
    {
        mAdapter.setData(data);
        mAdapter.notifyDataSetChanged();

    }

    @Override
    public void loadData(boolean pullToRefresh)
    {
        presenter.loadVenues(mSearchQuery, pullToRefresh);
    }

    @Override
    public void onRefresh()
    {
        loadData(true);
    }

    @Override
    public void onNoLocationProvider()
    {
        Toast.makeText(this, R.string.error_no_location_provider, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationServicesDisabled()
    {
        Toast.makeText(this, "Location services disabled", Toast.LENGTH_SHORT).show();
    }
}
