package com.manabreak.nordsquare.presenters;

import android.content.Context;
import android.location.Location;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.manabreak.nordsquare.Auth;
import com.manabreak.nordsquare.models.SearchResponse;
import com.manabreak.nordsquare.views.VenueView;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.utils.LocationState;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Presenter for venues.
 *
 * Created by Harri Pellikka on 9.11.2015.
 */
public class VenuesPresenter extends MvpBasePresenter<VenueView>
{
    private static final String BASE_URL = "https://api.foursquare.com/";
    private static final String API_VERSION = "v2";
    private static final String TAG = "VenuesPresenter";
    private static final String VERSION = "20151110";
    private static final String MODE = "foursquare";

    /**
     * Retrofit interface for Foursquare API
     */
    private interface FoursquareAPI
    {
        @GET(API_VERSION + "/venues/search")
        Call<SearchResponse> search(
                @Query("client_id") String clientID,
                @Query("client_secret") String clientSecret,
                @Query("v") String version,
                @Query("m") String mode,
                @Query("query") String query,
                @Query("ll") String location
        );
    }

    private final Context mContext;
    private final Retrofit mRetrofit;
    private final FoursquareAPI mAPI;
    private String mQueryCache = "";

    /**
     * Constructs a new presenter for Foursquare venues
     */
    public VenuesPresenter(Context context)
    {
        mContext = context;

        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mAPI = mRetrofit.create(FoursquareAPI.class);
    }


    /**
     * Loads the venues near the user based on the given query string
     * @param pullToRefresh Was the refresh caused by swipe-to-refresh?
     */
    public void loadVenues(final String query, final boolean pullToRefresh)
    {
        // If the query hasn't changed, don't do anything
        if(!pullToRefresh && query.equals(mQueryCache))
        {
            getView().showContent();
            return;
        }

        mQueryCache = query;

        getView().showLoading(pullToRefresh);

        SmartLocation sl = SmartLocation.with(mContext);
        LocationState state = sl.location().state();

        if(!state.isAnyProviderAvailable())
        {
            getView().onNoLocationProvider();
            return;
        }

        if(!state.locationServicesEnabled())
        {
            getView().onLocationServicesDisabled();
            return;
        }

        sl.location().oneFix().start(new OnLocationUpdatedListener()
        {
            @Override
            public void onLocationUpdated(Location location)
            {
                Call<SearchResponse> call = mAPI.search(Auth.FS_CLIENT_ID, Auth.FS_CLIENT_SECRET, VERSION, MODE, query, location.getLatitude() + "," + location.getLongitude());
                call.enqueue(new Callback<SearchResponse>()
                {
                    @Override
                    public void onResponse(Response<SearchResponse> response, Retrofit retrofit)
                    {
                        if(response.body() != null)
                        {
                            getView().setData(response.body().getVenues());
                        }
                        getView().showContent();
                    }

                    @Override
                    public void onFailure(Throwable t)
                    {
                        getView().showError(t, pullToRefresh);
                    }
                });
            }
        });
    }
}
