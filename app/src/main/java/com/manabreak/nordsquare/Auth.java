package com.manabreak.nordsquare;

/**
 * Auth-related constants.
 *
 * Created by Harri Pellikka on 10.11.2015.
 */
public class Auth
{
    public static final String FS_CLIENT_ID = "Client ID here";
    public static final String FS_CLIENT_SECRET = "Client secret here";
}
