package com.manabreak.nordsquare;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.manabreak.nordsquare.models.Venue;
import java.util.List;

/**
 * Adapter for listing venues in a recycler view.
 *
 * Created by Harri Pellikka on 10.11.2015.
 */
public class VenuesAdapter extends RecyclerView.Adapter<VenuesAdapter.ViewHolder>
{
    private List<Venue> mVenues;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_venue, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
    {
        final Venue v = mVenues.get(position);
        holder.mName.setText(v.getName());
        holder.mDistance.setText(v.getLocation().getFormattedDistance());

        String addr = v.getLocation().getAddress();
        if(addr == null || addr.isEmpty())
        {
            // No address, hide the address text view
            holder.mAddress.setVisibility(View.GONE);
        }
        else
        {
            holder.mAddress.setVisibility(View.VISIBLE);
            holder.mAddress.setText(v.getLocation().getAddress());
        }
    }

    @Override
    public int getItemCount()
    {
        return mVenues == null ? 0 :  mVenues.size();
    }

    /**
     * Sets the data to be shown with this adapter
     * @param venues List of venues
     */
    public void setData(List<Venue> venues)
    {
        mVenues = venues;
    }

    /**
     * View holder implementation
     */
    static class ViewHolder extends RecyclerView.ViewHolder
    {
        final TextView mName;
        final TextView mDistance;
        final TextView mAddress;

        public ViewHolder(View view)
        {
            super(view);
            mName = (TextView)view.findViewById(R.id.tvName);
            mDistance = (TextView)view.findViewById(R.id.tvDistance);
            mAddress = (TextView)view.findViewById(R.id.tvAddress);
        }
    }
}
