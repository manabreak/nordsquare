package com.manabreak.nordsquare.views;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;
import com.manabreak.nordsquare.models.Venue;
import java.util.List;

/**
 * View interface for venues.
 *
 * Created by Harri Pellikka on 9.11.2015.
 */
public interface VenueView extends MvpLceView<List<Venue>>
{
    void onNoLocationProvider();
    void onLocationServicesDisabled();
}
